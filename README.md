# Pre - requisitos

Descargar el proyecto.
ejecutar npm install
ejecutar ng serve
el programa comenzara en login

iniciar sesion con alguna cuenta o con la predeterminada adjuntada en el formulario, luego de loguearse, el sistema se redirigira a localhost:4700/contact, pero ese endpoint no esta implementado todavia.


Tambien puede dirigirse a localhost:4700/company para registrar una compañia, luego de registrar una compañia se redirigira a login para probar que si se completo el registro.


# Puntos implementados
  Modularización (Routing) (15 puntos).
  
  Lazy loading modules (10 puntos).
  
  Estrategias de detección de cambios (20 puntos).
  
  Encapsulación de componentes (5 puntos).
  
  Ciclo de vida de un componente (10 puntos)
  
  Servicios (5 puntos)
  
  LocalStorage (5 puntos)
  
  Formularios reactivos (10 puntos)
  
  POST. Create Company (account) (5 puntos)
  
  POST. Authentication (5 puntos)

