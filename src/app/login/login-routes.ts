import {Routes} from '@angular/router';
import {LoginFormComponent} from './login-form/login-form.component';

export const LOGIN_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: LoginFormComponent,
    children: [
    ]
  }
];
