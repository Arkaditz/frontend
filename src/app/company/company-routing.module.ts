import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {COMPANY_ROUTES_CONFIG} from './company-routes';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(COMPANY_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
