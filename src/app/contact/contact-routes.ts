import {Routes} from '@angular/router';
import {ContactFormComponent} from './contact-form/contact-form.component';

export const CONTACT_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: ContactFormComponent,
    children: [
    ]
  }
];
