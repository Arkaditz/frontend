import {Component, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {emailValidator} from './validators/email.validator';
import {Subscription} from 'rxjs';
import {LoginHttpService} from '../login-http.service';
import {AuthenticationService} from '../authentication.service';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnDestroy {

  public loginForm: FormGroup;

  public alertMessage: string;

  public readonly validDomains = ['gmail', 'hotmail'];
  public readonly passMinLength = 3;

  public thereAreErrors: boolean;

  private timeout;

  private loginSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private loginHttpService: LoginHttpService,
              private authService: AuthenticationService,
              private router: Router) {
    this.thereAreErrors = false;
    this.timeout = null;

    this.loginForm = this.formBuilder.group({
      username: [
        'company1@gmail.com',
        [
          Validators.required,
          emailValidator(this.validDomains)
        ]
      ],
      password: [
        '1234',
        [
          Validators.required,
          Validators.minLength(this.passMinLength)
        ]
      ]
    });
  }

  ngOnDestroy(): void {
    this.clearTimeout();

    this._unsubscribe(this.loginSubscription);
  }

  public onSubmit(): void {
    if (this.loginForm.valid) {
      this.loginSubscription = this.loginHttpService.doPost(this.loginForm.value).subscribe(
        (response: HttpResponse<any>) => {
          const TOKEN = response.headers.get('authorization');
          this.authService.setToken(TOKEN);
          this.router.navigate(['contact']);
        },
        () => {
          this.showAlert('User or password is not valid.');
        }
      );
    } else {
      this.showAlert('You must fix the errors to proceed.');
    }

  }

  private showAlert(content: string): void {
    this.alertMessage = content;
    this.clearTimeout();
    this.thereAreErrors = true;

    this.timeout = setTimeout(() => this.thereAreErrors = false, 2000);
  }

  private clearTimeout(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
